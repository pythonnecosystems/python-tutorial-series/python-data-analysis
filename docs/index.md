# Python 데이터 분석: Pandas, NumPy 라이브러리 <sup>[1](#footnote_1)</sup>

> <font size="3">Python에서 panda와 NumPy 라이브러리를 사용하여 데이터 분석을 수행하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./data-analysis.md#intro)
1. [데이터 분석이란?](./data-analysis.md#sec_02)
1. [데이터 분석을 위한 Python?](./data-analysis.md#sec_03)
1. [Pandas 시작하기](./data-analysis.md#sec_04)
1. [Panda로 데이터 조작](./data-analysis.md#sec_05)
1. [Panda로 데이터 시각화](./data-analysis.md#sec_06)
1. [NumPy 시작하기](./data-analysis.md#sec_07)
1. [NumPy로 배열 작업](./data-analysis.md#sec_08)
1. [NumPy로 선형대수](./data-analysis.md#sec_09)
1. [요약](./data-analysis.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 44 — Python Data Analysis: Pandas, NumPy Libraries](https://python.plainenglish.io/python-tutorial-44-python-data-analysis-pandas-numpy-libraries-808e5a5e393a?sk=4c2127eef45c500025a0c26b21ae5c83)를 편역하였습니다.
